package com.example.edt13;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {

    private ImageView imgDetail;
    private TextView titleDetail;
    private TextView authorDetail;
    private TextView descDetail;

    String title, author, desc, urlImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        imgDetail = findViewById(R.id.imgDetail);
        titleDetail = findViewById(R.id.titleDetail);
        authorDetail = findViewById(R.id.authorDetail);
        descDetail = findViewById(R.id.descDetail);

        getData();
        setData();
    }

    private void getData(){
        if(getIntent().hasExtra("title") && getIntent().hasExtra("author")){
            title = getIntent().getStringExtra("title");
            author = getIntent().getStringExtra("author");
            desc = getIntent().getStringExtra("desc");
            urlImg = getIntent().getStringExtra("urlImage2");
        } else {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData(){
        titleDetail.setText(title);
        authorDetail.setText(author);
        descDetail.setText(desc);

        Picasso.get().load(urlImg).fit().centerCrop().into(imgDetail);
    }
}